Name: Team 23 - ClickConnect

Logo: https://qubstudentcloud-my.sharepoint.com/personal/40177053_ads_qub_ac_uk/Documents/Microsoft%20Teams%20Chat%20Files/ClickConnectLogo.jpg

Description:  With the ongoing pandemic, the use of technology is vital in helping people with their remote shopping, users around the world will depend on their devices in order to do their important shopping, whether that is grocery’s, clothes, other household goods and entertainment.

Support: emccorry04@qub.ac.uk, egilvary01@qub.ac.uk, vwilson13@qub.ac.uk, aabduvalieva01@qub.ac.uk, ablakely03@qub.ac.uk, jmagill10@qub.ac.uk

Roadmap: Prototype 7th December

Contribution: In relation to the development of the project this is only applicable to members of Team 23.

Authors & Acknowledgement: Team 23 - Eimear McCorry, Beth Gilvary, Vanessa Wilson, Alina Abduvalieva, Anna Blakely, Jack Magill

Assistance/support: Austin Rainer, Daniel Keenan, Angela Allen

Project Status - Currently Active